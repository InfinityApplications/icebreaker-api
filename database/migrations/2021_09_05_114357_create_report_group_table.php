<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportGroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('reporting_user_id')->unsigned();
            $table->bigInteger('reported_group_id')->unsigned();
            $table->text('reason');
            $table->timestamps();

            $table->foreign('reporting_user_id')->references('id')->on('users');
            $table->foreign('reported_group_id')->references('id')->on('groups');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_groups');
    }
}
