<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMutualLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mutual_likes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id_one')->unsigned();
            $table->bigInteger('user_id_two')->unsigned();
            $table->timestamps();

            $table->foreign('user_id_one')->references('id')->on('users');
            $table->foreign('user_id_two')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mutual_likes');
    }
}
