<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->bigInteger('plan_id')->unsigned();
            $table->bigInteger('group_id')->unsigned();
            $table->rememberToken();
            $table->string('api_token')->nullable();
            $table->dateTime('date_of_birth');
            $table->text('bio')->nullable();
            $table->string('occupation')->nullable();
            $table->string('profile_picture_one', 100)->nullable();
            $table->string('profile_picture_two', 100)->nullable();
            $table->string('profile_picture_three', 100)->nullable();
            $table->enum('sex', ['man', 'woman', 'both']);
            $table->enum('sexual_orientation', ['straight', 'lesbian', 'bisexual', 'pansexual', 'gay', 'asexual', 'allosexual', 'heterosexual', 'homosexual', 'monosexual', 'polysexual', 'queer']);
            $table->string('location_name');
            $table->double('latitude');
            $table->double('longitude');
            $table->enum('looking_for', ['man', 'woman', 'both']);
            $table->integer('max_age')->default(99);
            $table->integer('min_age')->default(18);
            $table->bigInteger('proximity')->default(180);
            $table->timestamps();

            $table->foreign('group_id')->references('id')->on('groups');
            $table->foreign('plan_id')->references('id')->on('plans');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
