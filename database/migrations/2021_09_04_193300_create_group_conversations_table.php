<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGroupConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('group_conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('message')->nullable();
            $table->bigInteger('group_id_one')->unsigned();
            $table->bigInteger('group_id_two')->unsigned();
            $table->timestamps();

            $table->foreign('group_id_one')->references('id')->on('groups');
            $table->foreign('group_id_two')->references('id')->on('groups');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('group_conversations');
    }
}
