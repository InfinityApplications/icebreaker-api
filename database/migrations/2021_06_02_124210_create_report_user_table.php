<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('reporting_user_id')->unsigned();
            $table->bigInteger('reported_user_id')->unsigned();
            $table->text('reason');
            $table->timestamps();

            $table->foreign('reporting_user_id')->references('id')->on('users');
            $table->foreign('reported_user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_user');
    }
}
