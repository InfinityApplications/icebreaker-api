<?php

return [
    "api_url" => env('MESSAGE_API_URL'),
    "api_username" => env('MESSAGE_API_USERNAME'),
    "api_password" => env('MESSAGE_API_PASSWORD')
];
