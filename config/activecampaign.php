<?php

return [
    'api_key' => env('ACTIVE_CAMPAIGN_API_KEY'),
    'api_url' => env('ACTIVE_CAMPAIGN_BASE_URL'),
    'contact_list_id' => env('CONTACT_LIST_ID')
];