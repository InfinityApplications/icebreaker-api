<?php

namespace App\Http\Controllers;

use App\Http\Requests\API\DeleteProfileRequest;
use App\Http\Requests\API\ProfileUploadMediaRequest;
use App\Http\Requests\API\UpdateProfileRequest;
use App\Repositories\ProfileRepository;
use Illuminate\Routing\Controller as BaseController;

class ProfileController extends BaseController
{
    protected $profileRepository;

    /**
     * ActivityProviderController constructor.
     * @param null $activityProviderRepository
     */
    public function __construct($profileRepository = null)
    {

        if ($profileRepository === null) {
            $profileRepository = new ProfileRepository();
        }

        $this->profileRepository = $profileRepository;
    }

    public function update(UpdateProfileRequest $request)
    {
        $profileData = $request->all();
        unset($profileData['q']);
        return $this->profileRepository->update($profileData);
    }

    public function delete(DeleteProfileRequest $request)
    {
        return $this->profileRepository->delete($request->get('email'), $request->get('password'));
    }

    public function uploadMedia(ProfileUploadMediaRequest $request)
    {
        return $this->profileRepository->uploadMedia();
    }

}
