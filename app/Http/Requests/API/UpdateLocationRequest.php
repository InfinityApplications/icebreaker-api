<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;


class UpdateLocationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location_name' => 'required|string',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
        ];
    }
}
