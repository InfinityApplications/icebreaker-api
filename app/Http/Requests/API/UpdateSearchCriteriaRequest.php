<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;


class UpdateSearchCriteriaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'looking_for' => 'required|string',
            'max_age' => 'required',
            'min_age' => 'required',
            'status' => 'required|string',
            'proximity' => 'required',
        ];
    }
}
