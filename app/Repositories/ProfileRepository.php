<?php

namespace App\Repositories;

use App\Models\User;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProfileRepository
{

    public function get($userId)
    {

        $user = User::where('id', $userId)->first();

        if ($user === null) {
            throw new NotFoundHttpException();
        }

        return $user;
    }

    public function update($data)
    {
        User::where('id', auth()->user()->id)->update($data);

        return User::where('id', auth()->user()->id)->first();
    }

    public function delete()
    {
        $credentials = request(['email', 'password']);

        if (!$token = auth()->attempt($credentials)) {

            return response()->json(['message' => 'Failed to delete account'], 409);
        } else {
            User::where('id', auth()->user()->id)->delete();

            response()->json(['message' => 'You account has been deleted'], 401);
        }

    }

    public function uploadMedia()
    {
        //do later
    }
}
