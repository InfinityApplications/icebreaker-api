<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'email_verified_at',
        'password',
        'api_token',
        'date_of_birth',
        'bio',
        'occupation',
        'profile_picutres',
        'sex',
        'sexual_orientation',
        'location_name',
        'latitude',
        'longitude',
        'plan_id',
        'looking_for',
        'max_age',
        'min_age',
        'proximity',

    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $attributes = [
        'bio' => '',
        'occupation' => '',
        'profile_picutres' => '',
        'sexual_orientation' => 'straight',
        'looking_for' => 'both',
        'max_age' => 99,
        'min_age' => 18,
        'proximity' => 180,

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'profile_picutre' => 'array',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected $hidden = [
        "created_at",
        "updated_at",
        "stripe_id",
        "card_brand",
        "card_last_four",
        "trial_ends_at",
        'password',
        'remember_token',
        'api_token',
        "subscription_id",
        "location_id",
        "search_criteria_id",
    ];

    public static function getPossibleEnumValues($column)
    {
        // Create an instance of the model to be able to get the table name
        $instance = new static;

        $arr = DB::select(DB::raw('SHOW COLUMNS FROM ' . $instance->getTable() . ' WHERE Field = "' . $column . '"'));
        if (count($arr) == 0) {
            return array();
        }
        // Pulls column string from DB
        $enumStr = $arr[0]->Type;

        // Parse string
        preg_match_all("/'([^']+)'/", $enumStr, $matches);

        // Return matches
        return isset($matches[1]) ? $matches[1] : [];
    }

}
