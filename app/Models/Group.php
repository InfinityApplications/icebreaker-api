<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id_one',
        'user_id_two',
        'user_id_three',
        'user_id_four',
        'user_id_one_profile_picture',
        'user_id_two_profile_picture',
        'user_id_three_profile_picture',
        'user_id_four_profile_picture',
        'average_group_age',
        'average_group_gender',
        'swiping_in_user_id',
        'proximity',
        'minimum_average_group_age',
        'maximum_average_group_age',
        'maximum_average_group_age',
        'looking_for_group_average_gender',
    ];
}
