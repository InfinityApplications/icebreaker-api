<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLobby extends Model
{
    protected $table = 'user_lobby';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'status',
    ];

}
