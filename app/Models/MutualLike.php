<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MutualLike extends Model
{
    protected $table = 'mutual_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'user_id_one',
        'user_id_two',
    ];
}
