<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupDislike extends Model
{
    protected $table = 'group_dislikes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id',
        'group_id_disliked',
        'frequency',
    ];
}
