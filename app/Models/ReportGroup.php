<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReportGroup extends Model
{
    protected $table = 'report_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reporting_user_id',
        'reported_group_id',
        'reason',
    ];

}
