import json
import sys
from pprint import pprint

# read
jdata = open('./storage/keys/google.json', "r")
contents = jdata.read()

# write
converted = json.loads(contents)
jdata = open('./storage/keys/google.json', "w")
jdata.write(converted)

jdata.close()