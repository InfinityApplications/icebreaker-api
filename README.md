## Yonder API
Could be seen as the core of the application.
The API is responsible for connecting all moving parts so that information can be made sense by activity providers.

### Provisioning a local environment
Requires docker.

Just run: `docker-compose up`

### Deployment
See `bitbucket-pipelines.yml` and `Dockerfile.deploy`

### Documentation
Postman documentation is available at:
https://documenter.getpostman.com/view/1987706/RWTspFFW#intro

### Important third parties
- dingo/api - Makes managing APIs easier
- tymon/jwt-auth - Auth tokens
- zizaco/entrust - Roles and permissions management
- backpack - Generates an admin panel, we're using this whilst the actual panel ins't complete
- whytewaters/api-helper-library - RTBS wrapper


