<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

    $api->group(['middleware' => 'api', 'namespace' => 'App\Http\Controllers'], function ($api) {
        // /auth
        $api->group(['prefix' => 'auth'], function ($api) {
            $api->post('/login', 'AuthController@login');

            $api->post('/signup', 'AuthController@signup');

            $api->post('/reset-password', 'AuthController@resetPassword');

            $api->post('/me', 'AuthController@me');

        });

        $api->group(['prefix' => 'actions'], function ($api) {

            $api->post('/like/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@like']);

            $api->post('/dislike/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@dislike']);

            $api->post('/unmatch/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@unmatch']);

            $api->post('/report/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@report']);

            $api->post('/like/group/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@likeGroup']);

            $api->post('/dislike/group/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@dislikeGroup']);

            $api->post('/unmatch/group/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@unmatchGroup']);

            $api->post('/report/group/{id}', ['middleware' => 'auth:api', 'uses' => 'ActionController@reportGroup']);

            $api->get('/new-speed-date', ['middleware' => 'auth:api', 'uses' => 'ActionController@report']);

            $api->get('/new-group-video-chat', ['middleware' => 'auth:api', 'uses' => 'ActionController@report']);

        });

        $api->group(['prefix' => 'profile'], function ($api) {

            $api->get('/', ['middleware' => 'auth:api', 'uses' => 'ProfileController@get']);

            $api->patch('/', ['middleware' => 'auth:api', 'uses' => 'ProfileController@update']);

            $api->delete('/', ['middleware' => 'auth:api', 'uses' => 'ProfileController@delete']);

            $api->post('/upload-media', ['middleware' => 'auth:api', 'uses' => 'ProfileController@uploadMedia']);

        });

        $api->group(['prefix' => 'matches'], function ($api) {
            $api->get('/messages', ['middleware' => 'auth:api', 'uses' => 'MessageController@all']);

            $api->get('/messages', ['middleware' => 'auth:api', 'uses' => 'MessageController@getInbox']);

            $api->delete('/thread', ['middleware' => 'auth:api', 'uses' => 'MessageController@deleteMessageThread']);
        });

    });
});
